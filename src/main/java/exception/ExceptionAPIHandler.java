package exception;

import model.view.ResponseView;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionAPIHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(GeneralException.class)
    public final ResponseEntity<Object> handleAllExceptions(GeneralException ex) {
        ResponseView responseView = ResponseView.builder()
                .statusHttp(ex.getHttpStatus().value())
                .messageHttp(ex.getHttpStatus().getReasonPhrase())
                .build();

        if(ex instanceof ParametersMultipleErrorsException){
            List<GeneralException> exceptions = ((ParametersMultipleErrorsException) ex).getExceptions();
            responseView.setData(exceptions.stream().map(GeneralException::getMessage).collect(Collectors.toList()));
        }else{
            responseView.setData(ex.getMessage());
        }

        return ResponseEntity.status(ex.getHttpStatus()).body(responseView);
    }

}
