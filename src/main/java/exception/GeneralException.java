package exception;

import lombok.Data;
import model.LogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

@Data
public class GeneralException extends Exception {

    private HttpStatus httpStatus;
    private String message;

    private static final String errorMessage = "Error {} : {} ({})";

    protected GeneralException(HttpStatus httpStatus, String message){
        this.httpStatus = httpStatus;
        this.message = message;
    }

    protected void log(Class classObject, LogType logType){
        if(logType != null){
            Logger logger = LoggerFactory.getLogger(classObject);

            switch (logType){
                case ERROR:
                    logger.error(errorMessage, httpStatus.value(), message, classObject.getName());
                    break;
                case WARN:
                    logger.warn(errorMessage, httpStatus.value(), message, classObject.getName());
                    break;
                default:
                    logger.info(errorMessage, httpStatus.value(), message, classObject.getName());
            }
        }
    }

    @Override
    public String toString(){
        return this.httpStatus.value() + " : " + this.message;
    }

}
