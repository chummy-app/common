package model;

public enum LogType {

    INFO,
    ERROR,
    WARN

}
